﻿using Exam12019.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class RestaurantService
    {
        private readonly IDistributedCache Cache;

        public RestaurantService(IDistributedCache distributedCache)
        {
            this.Cache = distributedCache;
        }

        public async Task InsertRestaurantAsync(RestaurantViewModel rest)
        {
            var allRest = await GetRestaurantAsync();
            if (allRest == null)
            {
                allRest = new List<RestaurantViewModel>();
            }

            allRest.Add(rest);
            var restaurantJson = JsonConvert.SerializeObject(rest);
            await Cache.SetStringAsync("Restaurant", restaurantJson);
        }

        public async Task<List<RestaurantViewModel>> GetRestaurantAsync()
        {
            var valueJson = await Cache.GetStringAsync("Restaurant");
            if (valueJson == null)
            {
                return new List<RestaurantViewModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantViewModel>>(valueJson);
        }

        public async Task UpdateBookAsync(List<RestaurantViewModel> book)
        {
            var restaurantJson = JsonConvert.SerializeObject(book);
            await Cache.SetStringAsync("Restaurant", restaurantJson);
        }
    }
}
