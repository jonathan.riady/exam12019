﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Models
{
    public class RestaurantViewModel
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Address { set; get; }
    }
}
