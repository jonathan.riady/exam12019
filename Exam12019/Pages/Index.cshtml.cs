﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    public class IndexModel : PageModel
    {
        private readonly RestaurantService _BukuDbRedis;
        private readonly IHttpClientFactory _HttpFac;

        public IndexModel(RestaurantService bookService, IHttpClientFactory httpClientFactory)
        {
            this._BukuDbRedis = bookService;
            this._HttpFac = httpClientFactory;
        }
        [BindProperty]
        public List<RestaurantViewModel> Items { set; get; }

        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:49787/api/buku/";
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }

            Items = await respond.Content.ReadAsAsync<List<RestaurantViewModel>>();
            return Page();
        }
    }
}
