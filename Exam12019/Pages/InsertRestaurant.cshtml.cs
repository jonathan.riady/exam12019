using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages
{
    public class InsertRestaurantModel : PageModel
    {
        private readonly RestaurantService _BukuDbRedis;
        private readonly IHttpClientFactory _HttpFac;

        public InsertRestaurantModel(RestaurantService bookService, IHttpClientFactory httpClientFactory)
        {
            this._BukuDbRedis = bookService;
            this._HttpFac = httpClientFactory;
        }

        [TempData]
        public string SuccessMessage { set; get; }

        [BindProperty]
        public RestaurantViewModel FormRestaurant { set; get; }
        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:49787/api/restaurant";
            var client = _HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantViewModel
            {
                Id = Guid.NewGuid(),
                Name = FormRestaurant.Name,
                Address = FormRestaurant.Address,
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Insert via API Failed");
            }

            SuccessMessage = "Insert Berhasil";
            TempData["ErrorMessage"] = "Insert Gagal";

            return Page();
        }
    }
}