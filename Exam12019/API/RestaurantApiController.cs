﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Models;
using Exam12019.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exam12019.API
{
    [ApiController]
    [Route("api/restaurant")]
    public class KelolaBukuApiController : Controller
    {
        private readonly RestaurantService _RestaurantService;
        [BindProperty]
        public RestaurantViewModel FormRestaurant { set; get; }

        public KelolaBukuApiController(RestaurantService restaurantService)
        {
            this._RestaurantService = restaurantService;
        }

        [HttpPost(Name = "insert-restaurant")]
        public async Task<ActionResult> Post([FromBody]RestaurantViewModel model)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _RestaurantService.InsertRestaurantAsync(model);
            return Ok();
        }

        [HttpPost("{id}", Name = "update-restaurant")]
        public async Task<ActionResult> Update([FromBody]RestaurantViewModel model, Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _RestaurantService.GetRestaurantAsync();
            var findData = allData.FindIndex(Q => Q.Id == id);

            if (findData == -1)
            {
                return NotFound();
            }

            allData[findData] = model;

            await _RestaurantService.UpdateBookAsync(allData);
            return Ok();
        }

        [HttpDelete("{id}", Name = "delete-restaurant")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _RestaurantService.GetRestaurantAsync();
            var findData = allData.FindIndex(Q => Q.Id == id);

            if (findData == -1)
            {
                return NotFound();
            }

            allData.RemoveAt(findData);

            await _RestaurantService.UpdateBookAsync(allData);
            return Ok();
        }

        [HttpGet(Name = "get-all-data")]
        public async Task<ActionResult<List<RestaurantViewModel>>> GetAllData()
        {
            var allData = await _RestaurantService.GetRestaurantAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        [HttpGet("{id}", Name = "get-data-by-id")]
        public async Task<ActionResult<RestaurantViewModel>> GetDataById(Guid id)
        {
            var allData = await _RestaurantService.GetRestaurantAsync();
            var findData = allData.Where(Q => Q.Id == id).FirstOrDefault();

            if (findData == null)
            {
                return NotFound();
            }

            return findData;
        }
    }
}
